# ProxyList

Simple elixir app for get proxy list in format: type ip port

Made by Unix way so return simple text in spaceSeparateValues (ssv)

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `proxy_list` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:proxy_list, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/proxy_list](https://hexdocs.pm/proxy_list).

### As shell command

Build and excute

```shell
cp ./proxylist $XDG_BIN_HOME
```

or simple add proxy list to PATH vareable

## Build

```shell
mix escript.build
```

and excute

```shell
./proxy_list
```
