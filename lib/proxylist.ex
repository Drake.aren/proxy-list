defmodule ProxyList do
  @moduledoc """
  Get proxy list of Unix way
  """

  @doc """
  Return string of format: [type] [ip] [port]

  ## Examples

      iex> ProxyList.main()
      http 117.212.89.74 48797

  """
  def main(_) do
    data = HTTPoison.get!("https://www.free-proxy-list.net/")

    Floki.find(data.body, "tbody tr")
    |> Enum.map(fn e ->
      e
      |> Tuple.to_list()
      |> Enum.at(2)
      |> Enum.slice(0, 2)
      |> Enum.map(fn e1 ->
        e1 |> Tuple.to_list() |> Enum.at(2) |> List.first()
      end)
    end)
    |> Enum.map(fn e -> ["http"] ++ e end)
    |> Enum.map(fn e -> Enum.join(e, " ") end)
    |> Enum.join("\n")
    |> IO.puts()
  end
end
